import React, { Component } from 'react';
import { View, Text } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import List from './app/components/RestaurantList';
import Detail from './app/components/RestaurantDetail';
import Maps from './app/components/RestaunrantMaps';

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={List}
            options={{
              title: 'Restaurantes', headerStyle: { backgroundColor: '#E61A5F'},
              headerTitleStyle: {fontFamily: 'sans-serif-condensed',color: '#fff',fontSize: 50,},
            }}
          />
          <Stack.Screen
            name="Details"
            component={Detail}
            options={{title: 'Detalles',headerStyle: {backgroundColor: '#E61A5F'},
              headerTitleStyle: {fontFamily: 'sans-serif-condensed',color: '#fff',fontSize: 40,backgroundColor: '#E61A5F'},
            }}
          />
          <Stack.Screen
            name="Maps"
            component={Maps}
          />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}