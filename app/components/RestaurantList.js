import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    Image,
    ScrollView,
    TouchableOpacity

} from 'react-native';

class myList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lista: [
                {
                    id: "1",
                    titulo: "El Montonero",
                    informacion: "El Montonero ofrece una variada carta especializada en carnes a la parrilla. Nos distinguimos por la sazón, la calidad de los cortes.",
                    longitude: -71.5350079,
                    latitude: -16.3900007,
                    imagen: "https://media-cdn.tripadvisor.com/media/photo-s/03/1b/68/99/lazos-parrilla-de-zingaro.jpg",
                    estado: true,
                },
                {
                    id: "2",
                    titulo: "El Barril AQP",
                    informacion: "El Barril ofrece una variada carta especializada en carnes a la parrilla. Nos distinguimos por la sazón",
                    longitude: -71.54429,
                    latitude: -16.390018,
                    imagen: "https://media-cdn.tripadvisor.com/media/photo-s/0f/e4/c3/ab/el-barril-aqp.jpg",
                    estado: true,
                },
                {
                    id: "3",
                    titulo: "Santoro",
                    informacion: "Santoro ofrece una variada carta especializada en carnes a la parrilla. Nos distinguimos por la sazón",
                    longitude: -71.5447761,
                    latitude: -16.4062396,
                    imagen: "https://media-cdn.tripadvisor.com/media/photo-s/1b/06/20/47/inmeatwetrust.jpg",
                    estado: true,
                },
                {
                    id: "4",
                    titulo: "Pezcadores",
                    informacion: "SUn cebiche refrescante. Distinguendo la calidad del mar.",
                    longitude: -71.540469,
                    latitude: -16.408572,
                    imagen: "https://media-cdn.tripadvisor.com/media/photo-s/14/68/21/a3/delicious-ceviche.jpg",
                    estado: true,
                },
                
            ],
        };
    }
    keyExtractor = (item, index) => index.toString();

    renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Details', {
                itemId: item.id,
                itemImage: item.imagen,
                itemTitle: item.titulo,
                itemInfo: item.informacion,
                itemLat: item.latitude,
                itemLong: item.longitude,
                itemEst: item.estado
            })}>
            <View style={styles.Contenedor}>
                <View>
                    <Image
                        source={{ uri: item.imagen }}
                        style={styles.Imagen}
                        resizeMode="cover"
                    />
                </View>
                <View style={styles.descripcion1}>
                    <Text style={styles.Nombre}>{item.titulo}</Text>
                    <Text style={styles.Descripcion}>{item.informacion}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <FlatList
                        data={this.state.lista}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Details', {
                                    itemId: item.id,
                                    itemImage: item.imagen,
                                    itemTitle: item.titulo,
                                    itemInfo: item.informacion,
                                    itemLat: item.latitude,
                                    itemLong: item.longitude,
                                    itemEst: item.estado
                                })}>
                                <View style={styles.Contenedor}>
                                    <View>
                                        <Image
                                            source={{ uri: item.imagen }}
                                            style={styles.Imagen}
                                            resizeMode="cover"
                                        />
                                    </View>
                                    <View style={styles.descripcion1}>
                                        <Text style={styles.Nombre}>{item.titulo}</Text>
                                        <Text style={styles.Descripcion}>{item.informacion}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={this.keyExtractor}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        backgroundColor: '#C0C841',
        borderColor: 'red',
    },
    Contenedor: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        borderColor: 'red',
        justifyContent: 'flex-start',
        margin: 15,
    },
    Nombre: {
        fontSize: 25,
        fontWeight: 'bold',
        paddingRight: 68,
    },
    Descripcion: {
        fontSize: 14,
        color: 'gray',
        textAlign: 'justify',
        paddingRight: 68,
    },
    descripcion1: {
        marginLeft: 20,
        marginRight: 80,
    },
    Imagen: {
        width: 150,
        height: 135,
        borderRadius: 12,
    },
});
export default myList;
