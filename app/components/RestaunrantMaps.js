import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import {
    responsiveHeight,
    responsiveWidth,
} from 'react-native-responsive-dimensions';

const { width, height } = Dimensions.get('window');

const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class RestaurantUbication extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [{
                id: this.props.route.params.itemId,
                titulo: this.props.route.params.itemTitle,
                latitud: this.props.route.params.itemLat,
                longitud: this.props.route.params.itemLong,
            }],
            estado: this.props.route.params.itemEst,
        };
    }

    render() {
        const { latitud, longitud } = this.state.items;
        return (
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    provider={PROVIDER_GOOGLE}
                    initialRegion={
                        this.state.estado
                            ? {
                                latitude: latitud,
                                longitude: longitud,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }
                            : {
                                latitude: -16.4135013,
                                longitude: -71.530303,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }
                    }>
                    <Marker  
                        coordinate={
                            this.state.estado
                                ? {
                                    latitud: latitud,
                                    longitud: longitud,
                                }
                                : {
                                    latitude: -16.408572,
                                    longitude: -71.540469,
                                }
                        }>
                    </Marker>
                </MapView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    radius: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        overflow: 'hidden',
        backgroundColor: 'rgba(0,122,255,0.1)',
        borderWidth: 1,
        borderColor: 'rgba(0,122,255,0.3)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    marker: {
        height: 20,
        width: 20,
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: 20 / 2,
        overflow: 'hidden',
        backgroundColor: '#007AFF',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
    },
});